package ioasys.com.empresas_android.model

import java.io.Serializable

class Company (
    var name: String? = null,
    var type: String? = null,
    var country: String? = null
):Serializable