package ioasys.com.empresas_android.retrofit

import ioasys.com.empresas_android.model.Company
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CompanyService {

    @GET("/api/{api_version}/enterprises")
    fun getenterprises(
        @Header("access-token")accessToken: String,
        @Header("client")client: String,
        @Header("uid")uid: String,
        @Query("enterprise_types") enterpriseTypes: String,
        @Query("name")name: String
    ): Call<List<Company>>

}