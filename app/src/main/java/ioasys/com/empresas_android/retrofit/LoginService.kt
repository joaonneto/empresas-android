package ioasys.com.empresas_android.retrofit

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    @POST("api/{apiVersion}/users/auth/sign_in")
    fun login(@Body emailandpassword: Map<Any, Any>): Call<Void>
}