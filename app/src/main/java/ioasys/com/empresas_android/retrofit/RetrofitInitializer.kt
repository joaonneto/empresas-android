package ioasys.com.empresas_android.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitInitializer{
    private val retrofit =  Retrofit.Builder()
        .baseUrl("http://empresas.ioasys.com.br")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun loginservice(): LoginService{
        return retrofit.create(LoginService::class.java)
    }
}