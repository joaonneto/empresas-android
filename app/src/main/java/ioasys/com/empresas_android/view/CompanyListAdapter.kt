package ioasys.com.empresas_android.view

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ioasys.com.empresas_android.R
import ioasys.com.empresas_android.model.Company
import kotlinx.android.synthetic.main.adapter_list.view.*

class CompanyListAdapter (private val companies: List<Company>): RecyclerView.Adapter<CompanyListAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_list, parent, false))
    }

    override fun getItemCount(): Int {
        return companies.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vehicle = companies[position]
        holder.bindView(vehicle)
    }


    class ViewHolder(itemview: View): RecyclerView.ViewHolder(itemview){

        val companyName = itemview.textViewCompanyName
        val businesstype = itemview.textViewCompanyBusinessType
        val contrry = itemview.textViewCompanyCountry

        fun bindView(company: Company){



            itemView.setOnClickListener {
                val intent = Intent(it.context, CompanyActivity::class.java)


                it.context.startActivity(intent)
            }

        }
    }

}