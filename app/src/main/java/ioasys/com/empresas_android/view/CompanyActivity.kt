package ioasys.com.empresas_android.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import ioasys.com.empresas_android.R
import kotlinx.android.synthetic.main.activity_main.*

class CompanyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
}
}
