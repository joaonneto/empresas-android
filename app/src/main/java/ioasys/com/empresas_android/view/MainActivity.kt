package ioasys.com.empresas_android.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import ioasys.com.empresas_android.R
import ioasys.com.empresas_android.model.Company
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
      setUpView()

  }

    private fun setUpView(){

        recyclerViewList.adapter = CompanyListAdapter(listCompanies())
        recyclerViewList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


    }

    private fun listCompanies(): List<Company>{
        //Aqui entraria a requisição para receber a lista e monta o recyclerView
        return listOf()
    }
}

