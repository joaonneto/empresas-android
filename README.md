# README

Projeto para teste de conhecimentos da ioasys

## Estágio do Teste

Neste projeto não foi alcançado o resultado final do teste


## O que foi feito?

Construção dos layouts

Montagem do adapter do RecyclerView

Criação das interfaces do Retrofit

## O que não foi feito?

Teste das requisições com o Postman e com isso:
    
    Montar o modelo de classe para receber o JSON;
    
    Realizar Login;
    
    Realizar a Busca.
    
## Porque não foi feito?

Ao impotar as requisições no postman pelo arquivo collection, e modificar os parâmetros da requisição pelo quefoi passado no teste, a requisição retornou o código 404.

Foi alterado por conta própria algumas coisas na requisição, tentando deixar parecido com o padrão que utilizado nas requisições com Keycloak, porém,  também sem sucesso.

## Como testar?

Clone esse repositório para sua máquina

Abra o projeto com a IDE Android Studio: https://developer.android.com/studio

Conecte seu dispositivo Android em sua máquina via USB (Lembre de ativar o modo de depuração USB),  ou uitilize um dos emuladores disponíveis na IDE

Na Aba "Run", selecione "Run 'app'"

Espere o build

Utilize o app.

